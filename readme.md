Installing Dependencies
-----------------------

There are two ways to install the app, but both share the same step:
	- install pip: https://pip.pypa.io/en/stable/installing/

1. Using pipenv (Recomended)
	- Install pipenv: (see: https://docs.pipenv.org/)
		$ pip install pipenv
	- Install all dependencies from Pipfile
		$ pipenv install --skip-lock

2. Installing the necesary dependencies by hand
	- $ pip install Django==2.0.7 (see: https://www.djangoproject.com/download/)
	- $ pip install	django-crispy-forms
	- $ pip install django-allauth
	- $ pip install django-fobi
	- $ pip install ipython (optional)
	- $ pip install pipenv (optional)

Launching app in a local server
-------------------------------
If you installed the app following the step #1 from above, execute 
this commands in terminal:

	1. $ pipenv shell
	2. $ python manage.py runserver

NOTE refer docs folder to additional info

