from fobi.base import theme_registry

from fobi.contrib.themes.bootstrap3.fobi_themes import Bootstrap3Theme

__all__ = ('SeproFobiTheme',)


class SeproFobiTheme(Bootstrap3Theme):
    """Overriding the "bootstrap3" theme."""
    dashboard_template = 'fobi/generic/dashboard.html'
   
    master_base_template = 'fobi/bootstrap3/_base.html'
    base_template = 'fobi/generic/base.html'
    
    # edit_form_entry_ajax_template = 'fobi/generic/edit_form_entry_ajax.html'
    # form_snippet_template_name = 'fobi/generic/snippets/form_snippet.html'




# It's important to set the `force` argument to True, in
# order to override the original theme. Force can be applied
# only once.
theme_registry.register(SeproFobiTheme, force=True)
