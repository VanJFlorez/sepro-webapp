from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomePage.as_view(), name='home'),
    path('surveys/', views.all_public_surveys, name='all_public_surveys'),
    path('build_form/', views.build_form_gui, name='build_form_gui'),
    path('build_format/', views.build_format, name='build_format'),
    path('surveys/EncuestaCorta', views.encuesta_corta, name='encuesta_corta'),
    path('formato/infraestructura_1', views.infraestructura_1, name='infraestructura_1'),
    path('formato/infraestructura_2', views.infraestructura_2, name='infraestructura_2'),
    path('formato/operaciones', views.operaciones, name='operaciones'),
]
