from django.shortcuts import render, redirect
from django.views.generic.base import TemplateView
from . import fobi_themes

from fobi.base import get_theme
from fobi.models import (
    FormEntry,
    FormElementEntry,
    FormHandlerEntry,
    FormWizardEntry,
    FormWizardFormEntry,
    FormWizardHandlerEntry
)

from nine import versions

# FORMATOS --------------------------------------------------------------------
def infraestructura_1(request):
    if request.method == 'POST':
        printRequest(request)
        return redirect('infraestructura_2')
    return render(request, 'pages/build_format_infraestructura_1.html')

def infraestructura_2(request):
    if request.method == 'POST':
        printRequest(request)
        return redirect('home')
    return render(request, 'pages/build_format_infraestructura_2.html')

def operaciones(request):
    if request.method == 'POST':
        printRequest(request)
        return redirect('home')
    return render(request, 'pages/build_format_operaciones.html')

def build_format(request):
    return render(request, 'pages/build_format.html', {"context_var": "sample_text"})

# ENCUESTAS -------------------------------------------------------------------
def encuesta_corta(request):
    if request.method == 'POST':
        for entry in request.POST:
            print(entry)
    return render(request, 'pages/EncuestaCorta.html')

def build_form_gui(request):
    return render(request, 'pages/build_form_gui.html', {"context_var": "sample_text"})

# PAGES -----------------------------------------------------------------------
class HomePage(TemplateView):
    template_name = 'pages/home.html'

def all_public_surveys(request, theme=None, template_name=None):
    """all_public_surveys.

    :param django.http.HttpRequest request:
    :param fobi.base.BaseTheme theme: Theme instance.
    :param string template_name:
    :return django.http.HttpResponse:
    """
    form_entries = FormEntry.objects.all()

    context = {
        'form_entries': form_entries,
    }

    # If given, pass to the template (and override the value set by
    # the context processor.
    if theme:
        context.update({'fobi_theme': theme})

    if not template_name:
        theme = get_theme(request=request, as_instance=True)
        template_name = 'pages/surveys.html'

    if versions.DJANGO_GTE_1_10:
        return render(request, template_name, context)
    else:
        return render_to_response(
            template_name, context, context_instance=RequestContext(request)
        )

# NON VIEWS -------------------------------------------------------------------
def printRequest(request):
    if request.method == 'POST':
        for entry in sorted(request.POST):
            print(entry, request.POST[entry])
