/**
    A questionnaire is build up of FormElement instances. The id here is important,
    is used when an update of question numbering is required. Keep in mind
    that id and question number must be the same an consistent, because this is
    the key to retrieve and parse the data in the server.
*/
class FormElement {
  constructor(type, element, id, mainQuestion, depQuestionRef) {
    this.type = type;
    this.element = element;
    this.id = id;
    this.mainQuestion = mainQuestion;
    this.depQuestionRef = depQuestionRef;
  }

  createElement() {}
  updateId(id) { this.id = id; }
}
/*
  params:
    type        := integer
    element     := DOM HTML object
    id          := integer
    mainQuestion:= string
    cols        := integer
    rows        := integer
    colType...  := list of DOM HTML objects

*/
class TableElement extends FormElement {
  constructor(id, mainQuestion, cols, rows, colTypeInstances, headers, depQuestionRef) {
    super(null, null, id, mainQuestion, depQuestionRef);
    this.element = createTable(id, cols, rows, colTypeInstances, headers, depQuestionRef);
    this.cols = cols;
    this.rows = rows;
    this.colTypeInstances = colTypeInstances;
    this.headers = headers;
  }

  updateId(id) {
    this.id = id;
    this.element = createTable(id, this.cols, this.rows, this.colTypeInstances, this.headers, this.depQuestionRef);
  }
}

class InputTextElement extends FormElement {
  constructor(type, id, mainQuestion, depQuestionRef) {
    super(type, null, id, mainQuestion, depQuestionRef);
    this.element = createInputText(type, id, depQuestionRef);
  }

  updateId(id) {
    this.id = id;
    this.element = createInputText(this.type, id, this.depQuestionRef);
  }
}

class SelectionElement extends FormElement {
  constructor(type, id, mainQuestion, options, depQuestionRef) {
    super(type, null, id, mainQuestion, depQuestionRef);   // (type, element, id, mainQuestion, depQuestionRef)
    this.element = createSelection(type, id, options, depQuestionRef);
    this.options = options;
  }

  updateId(id) {
    this.id = id;
    this.element = createSelection(this.type, id, this.options, this.depQuestionRef);
  }
}

class SelectionYesNo extends FormElement {
  constructor(id, mainQuestion, hasThirdOption, depQuestionRef) {
    super(null, null, id, mainQuestion, depQuestionRef); // (type, element, id, mainQuestion)
    this.element = createSelectionYesNo(id, hasThirdOption);
    this.hasThirdOption = hasThirdOption;
  }

  updateId(id) {
    this.id = id;
    this.element = createSelectionYesNo(id, this.hasThirdOption);
  }
}

class Survey {
    constructor() {
        this.form = []; // list of DOM elements
        this.branchQuestionsIds = []; // list of integers
    }

    addFormElement(formElement) {
        this.form.push(formElement);
        if(formElement.element.getAttribute(ATTRIBUTE_TYPE_NAME) == ATTRIBUTE_TYPE_NAME__YES_NO_QUESTION)
            this.branchQuestionsIds.push(formElement.id);
    }

    deleteFormElement(pos) { // pos starts from 0
        this.form.splice(pos, 1);
        this.branchQuestionsIds = [];
        for(let i = 0; i < this.form.length; i++) {
            this.form[i].updateId(i + 1); // formElment.id = form_index + 1
            if(this.form[i].element.getAttribute(ATTRIBUTE_TYPE_NAME) == ATTRIBUTE_TYPE_NAME__YES_NO_QUESTION)
                this.branchQuestionsIds.push(i + 1);
        }
        CURRENT_ID = this.form.length;
    }
}
