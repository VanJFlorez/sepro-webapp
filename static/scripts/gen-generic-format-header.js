function generateGenericFormatHeader(formatName, description) {
    let head = document.createElement("div");
    head.setAttribute("class", "row row-eq-height");
    head.setAttribute("style", "border:1px solid black; margin-top: 15px; height: 90px;");

    let logos = document.createElement("div");
    logos.setAttribute("class", "col-sm-4");
    logos.setAttribute("style", "border:1px solid black;");
        let image1 = document.createElement("img");
        image1.setAttribute("src", "../static/images/corhuila.jpeg");
        image1.setAttribute("width", "100px");
        logos.appendChild(image1);
        let image2 = document.createElement("img");
        image2.setAttribute("src", "../static/images/un.png");
        image2.setAttribute("width", "100px");
        logos.appendChild(image2);
        let image3 = document.createElement("img");
        image3.setAttribute("src", "../static/images/sepro_h.png");
        image3.setAttribute("width", "100px");
    logos.appendChild(image3);
    head.appendChild(logos);

    let mainName = document.createElement("div");
    mainName.setAttribute("class", "col-sm-3");
    mainName.setAttribute("style", "border:1px solid black; text-align: center; vertical-align: middle;");
    mainName.appendChild(document.createTextNode(formatName));
    head.appendChild(mainName);

    let detail = document.createElement("div");
    detail.setAttribute("class", "col-sm-4");
    detail.setAttribute("style", "border:1px solid black; text-align: center; vertical-align: middle;");
    detail.appendChild(document.createTextNode(description));
    head.appendChild(detail);

    let date = document.createElement("div");
    date.setAttribute("class", "col-sm-1");
    date.setAttribute("style", "padding: 0%; border:1px solid black;");
    head.appendChild(date);
        let field1 = document.createElement("div");
        field1.setAttribute("style", "text-align: center;");
        field1.appendChild(document.createTextNode("CD-2018"));
        date.appendChild(field1);
        let field2 = document.createElement("div");
        field2.setAttribute("style", "border-top:1px solid black; text-align: center;");
        field2.appendChild(document.createTextNode("VERSION 1"));
        date.appendChild(field2);
        let field3 = document.createElement("div");
        field3.setAttribute("style", "border-top:1px solid black; text-align: center;");
        field3.appendChild(document.createTextNode("16/03/18"));
        date.appendChild(field3);

    return head;
}
