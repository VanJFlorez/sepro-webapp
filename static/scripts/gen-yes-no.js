/**
 * Builds in the build_form_gui.html file the ui for create yesno answared questions...
 * @param {Node} node - Node where DOM elements from this editor will be appended
 */
function inputEditorYesNo(node) {
    let label = document.createElement("label");
    label.setAttribute("for", YESNO_EDITOR__HAS_THIRD_OPTION__KEY);
    label.appendChild(document.createTextNode("No sabe/No responde?: "));
    node.appendChild(label);
    node.appendChild(document.createElement("br"));

    let checkbox = document.createElement("input");
    checkbox.setAttribute("type", "checkbox");
    checkbox.setAttribute("id", YESNO_EDITOR__HAS_THIRD_OPTION__KEY);
    checkbox.setAttribute("class", FORM_EDITOR__GENERIC_VALUE__CLASS);
    checkbox.setAttribute("name", YESNO_EDITOR__HAS_THIRD_OPTION__KEY);
    checkbox.setAttribute("onclick", "inputEditorYesNo_aux(this)")
    node.appendChild(checkbox);
    node.appendChild(document.createElement("br"));
}
/**
 *  Auxiliary function to handle the check button in the editor...
 *  Only called by inputEditorYesNo()
 */
function inputEditorYesNo_aux(node) {
    /* Note: if value attribute exists in this element, has the specified value
     *       otherwise has the "on";
     */
    if (node.value == "true")
        node.removeAttribute("value");
    else
        node.setAttribute("value", "true");
}
/**
 * Bottleneck function that takes input given by user in element editor and
 * then builds the associated instance for that element... Then appends both
 * the HTML DOM element and element javascript class instance to the DOM tree
 * and global survey array of instances respectively...
 */
function buildYesNo() {
   CURRENT_ID = CURRENT_ID + 1;
    let values = getValuesFromEditor();

    let question = values.get(QUESTION_KEY);
    let hasThirdOption = values.get(YESNO_EDITOR__HAS_THIRD_OPTION__KEY);

    let body = makeRow(CURRENT_ID, question);
    let yesno = new SelectionYesNo(CURRENT_ID, question, hasThirdOption);

    body.appendChild(yesno.element);
    survey.addFormElement(yesno);
}
/**
  return a <div></div> DOM HTML element that contains two radio inputs
*/
function createSelectionYesNo(baseId, hasThirdOption) {
    var id = baseId;
    var formElement = document.createElement("div");
    formElement.setAttribute(ATTRIBUTE_TYPE_NAME, ATTRIBUTE_TYPE_NAME__YES_NO_QUESTION);
    // yes
    var input = document.createElement("input");
    input.setAttribute("type","radio");
    input.setAttribute("name", id);
    input.setAttribute("value", "Si");
    input.setAttribute("data-a", "expand");
    input.setAttribute("onchange", "radio_dependences(this)");
    input.setAttribute("questionNumber", id);
    formElement.appendChild(input);
    formElement.appendChild(document.createTextNode("Si"));
    formElement.appendChild(document.createElement("br"));
    // no
    input = document.createElement("input");
    input.setAttribute("type","radio");
    input.setAttribute("name", id);
    input.setAttribute("value", "No");
    input.setAttribute("data-a", "null");
    input.setAttribute("onchange", "radio_dependences(this)");
    input.setAttribute("questionNumber", id);
    formElement.appendChild(input);
    formElement.appendChild(document.createTextNode("No"));
    formElement.appendChild(document.createElement("br"));

    if (hasThirdOption == "true") {
        input = document.createElement("input");
        input.setAttribute("type", "radio");
        input.setAttribute("name", id);
        input.setAttribute("value", "No Sabe/No responde");
        input.setAttribute("data-a", "null");
        input.setAttribute("onchange", "radio_dependences(this)");
        input.setAttribute("questionNumber", id);
        formElement.appendChild(input);
        formElement.appendChild(document.createTextNode("No sabe/No Responde"));
        formElement.appendChild(document.createElement("br"));
    }

    return formElement;
}
