// Formato Inventario Infraestructura y Establecimientos (1 de 2)

(function generateFormatEstablecimientosMap() {
    let canvas = document.getElementById("format_canvas");
    // canvas.innerHTML = "";

    // head -------------------------------------------------------------------
    let header = generateGenericFormatHeader("FORMATO DE INVENTARIO DE ESTABLECIMIENTOS", "CARACTERIZACIÓN Y ANÁLISIS DEL CARGUE Y DESCARGUE DE MERCANCÍAS EN EL CENTRO DE LA CIUDAD DE NEIVA PARA LA IDENTIFICACIÓN DE ALTERNATIVAS PARA MEJORAR LA MOVILIDAD");
    canvas.appendChild(header);

    // pre body ---------------------------------------------------------------
    let prebody = formatPreBody();
    canvas.appendChild(prebody);

    // BODY -------------------------------------------------------------------
    let body = formatBody();
    canvas.appendChild(body);

    // OBSERVACIONES ----------------------------------------------------------
    let obs = formatObservations();
    canvas.appendChild(obs);

    // STREET SIGNALS ---------------------------------------------------------
    let strSign = formatStreetSignals();
    canvas.appendChild(strSign);

    // footer -----------------------------------------------------------------
    let foot = formatFoot();
    canvas.appendChild(foot);

    // submit button ----------------------------------------------------------
    let btn = formatSubmitButton();
    canvas.appendChild(btn);
})();

function formatPreBody() {
    let prebody = document.createElement("div");
    prebody.setAttribute("class", "row");
    prebody.setAttribute("style", "margin-top: 15px");

    prebody.appendChild(document.createTextNode("Tramo"));
    let tramo = document.createElement("input");
    tramo.setAttribute("type", "text");
    tramo.setAttribute("name", "1_1_tramo");
    tramo.setAttribute("style", "margin: 30px");
    prebody.appendChild(tramo);

    prebody.appendChild(document.createTextNode("entre"));
    let entre = document.createElement("input");
    entre.setAttribute("type", "text");
    entre.setAttribute("name", "1_2_inicio")
    entre.setAttribute("style", "margin: 30px");
    prebody.appendChild(entre);

    prebody.appendChild(document.createTextNode("y"));
    let and = document.createElement("input");
    and.setAttribute("type", "text");
    and.setAttribute("name", "1_3_fin");
    and.setAttribute("style", "margin: 30px");
    prebody.appendChild(and);

    prebody.appendChild(document.createTextNode("fecha"));
    let date2 = document.createElement("input");
    date2.setAttribute("type", "date");
    date2.setAttribute("name", "1_4_fecha");
    prebody.appendChild(date2);

    return prebody;
}
function formatBody(){
    let outerLimit = document.createElement("div");
    outerLimit.setAttribute("style", "border:1px solid black; margin-top: 15px; text-align: center; ");
    outerLimit.setAttribute("class", "row");

    let title = document.createElement("div");
    title.setAttribute("style", "border-bottom: 1px solid black; margin-bottom: 30px; font-weight: bold;");
    title.appendChild(document.createTextNode("INVENTARIO DE INFRAESTRUCTURA VIAL"));
    outerLimit.appendChild(title);

    let canvasWrapper = document.createElement("div");
    canvasWrapper.setAttribute("width", "100%");
    canvasWrapper.setAttribute("id", "canvas-container");
    outerLimit.appendChild(canvasWrapper);

    let canvas = document.createElement("canvas");
    canvas.setAttribute("id", "c");
    canvas.setAttribute("width", "1200px");
    canvas.setAttribute("height", "900px");
    canvasWrapper.appendChild(canvas);

    let buttons = ["group", "ungroup", "multiselect", "discard", "remove", "btndownload"];
    for (let i = 0; i < buttons.length; i++) {
        let btn = document.createElement("button");
        btn.setAttribute("id", buttons[i]);
        btn.setAttribute("style", "margin-top: 15px;");
        btn.appendChild(document.createTextNode(buttons[i]));
        outerLimit.appendChild(btn);
    }

    return outerLimit;
}
function formatObservations() {
    let observationsRow = document.createElement("div");
    observationsRow.setAttribute("class", "row");
    observationsRow.setAttribute("style", "margin-top: 15px");

        let observations = document.createElement("textarea");
        observations.setAttribute("style", "width: 100%; height: 160px");
        observations.setAttribute("name", "3_observaciones")
        observations.setAttribute("placeholder", "OBSERVACIONES");
        observationsRow.appendChild(observations);

    return observationsRow;
}
function formatStreetSignals() {
    let row = document.createElement("div");
    row.setAttribute("class", "row row-eq-height");
    row.setAttribute("style", "border:1px solid black; margin-top: 15px;");

    let div1 = document.createElement("div");
    div1.setAttribute("class", "col-sm-8");
    div1.setAttribute("style", "padding: 0%;")
    row.appendChild(div1);
        let label1 = document.createElement("div");
        label1.setAttribute("style", "border-bottom: 1px solid black; font-weight: bold; text-align: center;");
        label1.appendChild(document.createTextNode("Da click sobre la señal para añadirla al mapa:"))
        div1.appendChild(label1);
        for (let i = 1; i < 9; i++) {
            let img = document.createElement("img");
            img.setAttribute("src", "../static/images/street_signals/" + i + ".jpeg");
            img.setAttribute("style", "width: 12.5%; padding: 0%;");
            img.setAttribute("id", "street-signal-" + i)
            div1.appendChild(img);
        }

    let div2 = document.createElement("div");
    div2.setAttribute("class", "col-sm-2");
    div2.setAttribute("style", "border-left: 1px solid black; padding: 0%;")
    row.appendChild(div2);
        let label2 = document.createElement("div");
        label2.setAttribute("style", "border-bottom: 1px solid black; font-weight: bold; text-align: center;");
        label2.appendChild(document.createTextNode("TIPO PAVIMENTO"))
        div2.appendChild(label2);
        let option = ['Rigido', 'Flexible', 'Articulado'];
        for (i = 0; i < option.length; i++) {
            let input = document.createElement("input");
            input.setAttribute("type", "radio");
            input.setAttribute("name", "4_1_tipo_pavimento");
            input.setAttribute("style", "margin-left: 10px;");
            input.setAttribute("value", option[i]);
            div2.appendChild(input);
            div2.appendChild(document.createTextNode(option[i]));
            div2.appendChild(document.createElement("br"));
        }
    let div3 = document.createElement("div");
    div3.setAttribute("class", "col-sm-2");
    div3.setAttribute("style", "border-left: 1px solid black; padding: 0%;")
    row.appendChild(div3);
        let label3 = document.createElement("div");
        label3.setAttribute("style", "border-bottom: 1px solid black; font-weight: bold; text-align: center;");
        label3.appendChild(document.createTextNode("ESTADO PAVIMENTO"))
        div3.appendChild(label3);
        option = ['Bueno', 'Regular', 'Malo']; // if u redefine option here the app will crash... and compiler do not will output warnings
        for (i = 0; i < option.length; i++) {
            let input = document.createElement("input");
            input.setAttribute("type", "radio");
            input.setAttribute("name", "4_2_estado_pavimento");
            input.setAttribute("style", "margin-left: 10px;");
            input.setAttribute("value", option[i]);
            div3.appendChild(input);
            div3.appendChild(document.createTextNode(option[i]));
            div3.appendChild(document.createElement("br"));
        }

    return row;
}
function formatFoot() {
    let footDiv = document.createElement("div");
    footDiv.setAttribute("style", "border:1px solid black; margin-top: 15px;");
    footDiv.setAttribute("class", "row");

    let dummyDiv = document.createElement("div");
    dummyDiv.setAttribute("class", "col-sm-1");
    footDiv.appendChild(dummyDiv);

    let aforadorWrapper = document.createElement("div");
    aforadorWrapper.setAttribute("class", "col-sm-4");
    aforadorWrapper.setAttribute("style", "padding: 0%; margin: 5px");
    footDiv.appendChild(aforadorWrapper);
    let aforador = document.createElement("div");
    aforador.setAttribute("style", "border: 1px solid black;")
    aforadorWrapper.appendChild(aforador);
    let inputAforador = document.createElement("textarea");
    inputAforador.setAttribute("name", "5_1_aforador");
    inputAforador.setAttribute("style", "border: 0; height: 60px; width: 100%;")
    inputAforador.setAttribute("placeholder", "AFORADOR:")
    aforador.appendChild(inputAforador);

    let supervisorWrapper = document.createElement("div");
    supervisorWrapper.setAttribute("class", "col-sm-4");
    supervisorWrapper.setAttribute("style", "padding: 0%; margin: 5px;");
    footDiv.appendChild(supervisorWrapper);
    let supervisor = document.createElement("div");
    supervisor.setAttribute("style", "border:1px solid black;");
    supervisorWrapper.appendChild(supervisor);
    let inputSupervisor = document.createElement("textarea");
    inputSupervisor.setAttribute("name", "5_2_supervisor");
    inputSupervisor.setAttribute("style", "border: 0; height: 60px; width: 100%;")
    inputSupervisor.setAttribute("placeholder", "SUPERVISOR:")
    supervisor.appendChild(inputSupervisor);

    let upzWrapper = document.createElement("div");
    upzWrapper.setAttribute("class", "col-sm-2");
    upzWrapper.setAttribute("style", "padding: 0%; margin: 5px;")
    footDiv.appendChild(upzWrapper);
    let upz = document.createElement("div");
    upz.setAttribute("style", "border:1px solid black;");
    upzWrapper.appendChild(upz);
    let inputUpz = document.createElement("textarea");
    inputUpz.setAttribute("name", "5_3_upz");
    inputUpz.setAttribute("style", "border: 0; height: 60px; width: 100%;")
    inputUpz.setAttribute("placeholder", "UPZ:");
    upz.appendChild(inputUpz);

    return footDiv;
}
function formatSubmitButton() {
    let row = document.createElement("div");
    row.setAttribute("class", "row");
    row.setAttribute("style", "margin: 15px;");

    let button = document.createElement("button");
    button.setAttribute("type", "submit");
    button.appendChild(document.createTextNode("Enviar"));
    button.onclick = function() {
        alert("Su repuesta se ha almacenado. \nGracias por participar!")
    }
    row.appendChild(button);

    return row;
}

(function () {
    var $ = function(id){return document.getElementById(id)};
    var canvas = this.__canvas = new fabric.Canvas('c');

    let img = document.createElement("img");
    img.setAttribute("src", "../static/images/road_map.png");

    canvas.setWidth($("canvas-container").clientWidth);
    canvas.setBackgroundImage(img.src, canvas.renderAll.bind(canvas), {
           scaleX: canvas.width/img.width
    });

    fabric.Object.prototype.transparentCorners = false;

    var group = $('group'),
      ungroup = $('ungroup'),
      multiselect = $('multiselect'),
      addmore = $('addmore'),
      discard = $('discard'),

      remove = $('remove');
      for (let i = 1; i < 9; i++) {
            let streetSignal = document.getElementById("street-signal-" + i);
            streetSignal.onclick = function() {
                fabric.Image.fromURL('../static/images/street_signals/' + i + '.jpeg', function(myImg) {
                    let img1 = myImg.set({ left: 0, top: 100});
                    img1.scaleToWidth(100);
                    img1.scaleToHeight(100);
                    canvas.add(img1);
                });
            }
      }
      multiselect.onclick = function() {
        canvas.discardActiveObject();
        var sel = new fabric.ActiveSelection(canvas.getObjects(), {
          canvas: canvas,
        });
        canvas.setActiveObject(sel);
        canvas.requestRenderAll();
      }

      group.onclick = function() {
        if (!canvas.getActiveObject()) {
          return;
        }
        if (canvas.getActiveObject().type !== 'activeSelection') {
          return;
        }
        canvas.getActiveObject().toGroup();
        canvas.requestRenderAll();
      }

      ungroup.onclick = function() {
        if (!canvas.getActiveObject()) {
          return;
        }
        if (canvas.getActiveObject().type !== 'group') {
          return;
        }
        canvas.getActiveObject().toActiveSelection();
        canvas.requestRenderAll();
      }

      discard.onclick = function() {
        canvas.discardActiveObject();
        canvas.requestRenderAll();
      }

      remove.onclick = function() {
          obj = canvas.getActiveObject();
          canvas.remove(obj);
      }


      function download(url,name){
      // make the link. set the href and download. emulate dom click
        $('road_map_link').attr({href:url,download:name}); //.click();
      }
      function downloadFabric(canvas,name){
        //  convert the canvas to a data url and download it.
        download(canvas.toDataURL(), name + '.png');
        // var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");  // here is the most important part because if you dont replace you will get a DOM 18 exception.
        // window.location.href=image; // it will save locally
      }

      var dwn = document.getElementById('btndownload');
      dwn.onclick = function(){
        downloadFabric(canvas, 'road_map.png');
      }
})();
