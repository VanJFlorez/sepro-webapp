// FORMATO_OPERACIONES_CD_V1

(function genFormatoOperaciones() {
    let canvas = document.getElementById("format_canvas");

    let headings = ['ID', 'ESTACIONAMIENTO', 'OPERACIONC/D', 'PLACA VEHICULO', 'TIPO VEHICULO',
                    'LUGAR ESTACIONAMIENTO', 'COSTADO', 'ANGULO', 'BODEGA', 'GENERADOR RECEPTOR',
                    'TIPO CARGA', 'EMPAQUE', 'UBICACIÓN CARGA', 'OCUPACIÓN DE LA CARGA', 'PERSONAS INVOLUCRADAS',
                    'HERRAMIENTAS', 'PROCESOS ADICIONALES', 'C', 'D', 'OPERACION C/D', 'ESTACIONAMIENTO', 'OBSERVACIONES'];
    let columnQuantity = 22; // prompt();
    let rowQuantity = 21; // prompt();

    // HEAD -------------------------------------------------------------------
    let header = generateGenericFormatHeader("FORMATO DE INVENTARIO DE OPERACIONES DE C/D", "CARACTERIZACIÓN Y ANÁLISIS DEL CARGUE Y DESCARGUE DE MERCANCÍAS EN EL CENTRO DE LA CIUDAD DE NEIVA PARA LA IDENTIFICACIÓN DE ALTERNATIVAS PARA MEJORAR LA MOVILIDAD");
    canvas.appendChild(header);

    // PREBODY ----------------------------------------------------------------
    let prebody = generateOperationPrebody();
    canvas.appendChild(prebody);

    // BODY -------------------------------------------------------------------
    let limite = document.createElement("div"); // div that holds column divs
    limite.setAttribute("class", "row");
    limite.setAttribute("style", "margin: 5px;"); // Fix: no colorea!
    canvas.appendChild(limite);

    let table = document.createElement("table");
    limite.appendChild(table);


        let headerGroupings = document.createElement("tr");
        table.appendChild(headerGroupings);

            let space = document.createElement("th");
            space.setAttribute("colspan", "1");
            headerGroupings.appendChild(space);

            let initHours = document.createElement("th");
            initHours.setAttribute("colspan", "2");
            initHours.setAttribute("scope", "colgroup");
            initHours.setAttribute("style", "text-align: center;")
            initHours.appendChild(document.createTextNode("HORA DE INICIO"));
            headerGroupings.appendChild(initHours);

            let vehicleParking = document.createElement("th");
            vehicleParking.setAttribute("colspan", "6");
            vehicleParking.setAttribute("scope", "colgroup");
            vehicleParking.setAttribute("style", "text-align: center;");
            vehicleParking.appendChild(document.createTextNode("ESTACIONAMIENTO DEL VEHICULO"));
            headerGroupings.appendChild(vehicleParking)

            let load = document.createElement("th");
            load.setAttribute("colspan", "5");
            load.setAttribute("scope", "colgroup");
            load.setAttribute("style", "text-align: center;");
            load.appendChild(document.createTextNode("CARGA"));
            headerGroupings.appendChild(load)

            let operation = document.createElement("th");
            operation.setAttribute("colspan", "5");
            operation.setAttribute("scope", "colgroup");
            operation.setAttribute("style", "text-align: center;");
            operation.appendChild(document.createTextNode("OPERACIÓN CARGUE/DESCARGUE (C/D)"));
            headerGroupings.appendChild(operation);

            let endHour = document.createElement("th");
            endHour.setAttribute("colspan", "2");
            endHour.setAttribute("scope", "colgroup");
            endHour.setAttribute("style", "text-align: center;");
            endHour.appendChild(document.createTextNode("HORA DE FINALIZACIÓN"));
            headerGroupings.appendChild(endHour);

    for(let i = 0; i < rowQuantity; i++) {
        let row = document.createElement("tr");
        table.appendChild(row);
        for(let j = 0; j < columnQuantity; j++) {
            if (i == 0) { // headers
                let colName = document.createElement("th");
                colName.appendChild(document.createTextNode(headings[j]));
                colName.setAttribute("style", "text-align: center; font-size: 10px; padding: 5px;");
                row.appendChild(colName);
                continue;
            }

            let entry = document.createElement("td");
            row.appendChild(entry);

            let input = document.createElement("input");
            input.setAttribute("name", "name");
            if (i%2 == 0)
                input.setAttribute("style", "border: 0; width: 100%; background-color: #E4E4E4;")
            else
                input.setAttribute("style", "border: 0; width: 100%;");

            switch (j) { // puts inside of each entry the correct input type
                case 0: // id column
                    entry.appendChild(document.createTextNode(i.toString()));
                    entry.setAttribute("style", "text-align: center;");
                    break;
                case 1: // estacionamiento column
                case 2: // operacion c/d column
                case 19:
                case 20:
                    input.setAttribute("type", "time");
                    input.setAttribute("name", "table_" + i + "_" + j + "_" + headings[j]);
                    entry.appendChild(input);
                    break;
                case 8:
                    input.setAttribute("type", "checkbox");
                    input.setAttribute("value", "alfa");
                    entry.setAttribute("style", "background-color: #E4E4E4;")
                    input.setAttribute("name", "table_" + i + "_" + j + "_" + headings[j]);
                    entry.appendChild(input);
                    break;
                case 21:
                    let attr = input.getAttribute("style");
                    input.setAttribute("style", attr + "width: 300px;")
                default:
                    input.setAttribute("name", "table_" + i + "_" + j + "_" + headings[j]);
                    input.setAttribute("type", "text");
                    entry.appendChild(input);
            }
            row.appendChild(entry);
        }
    }

    // FOOT -------------------------------------------------------------------
    let footRow = document.createElement("div");
    footRow.setAttribute("class", "row");
    canvas.appendChild(footRow);

        let tableWrapper1 = document.createElement("div");
        footRow.appendChild(tableWrapper1);
        let table1 = document.createElement("img");
        table1.setAttribute("src", "../static/images/convention.png");
        table1.setAttribute("height", "210px");
        //table1.setAttribute("class", "col-sm-4");
        tableWrapper1.appendChild(table1);

    // OBSERVACIONES ----------------------------------------------------------
    let observationsRow = document.createElement("div");
    observationsRow.setAttribute("class", "row");
    observationsRow.setAttribute("style", "margin-top: 15px");
    canvas.appendChild(observationsRow);

        let observations = document.createElement("textarea");
        observations.setAttribute("style", "width: 100%; height: 160px");
        observations.setAttribute("placeholder", "OBSERVACIONES");
        observations.setAttribute("name", "2_observaciones");
        observationsRow.appendChild(observations);

    // SUBMIT button ----------------------------------------------------------
    let submitRow = document.createElement("div");
    submitRow.setAttribute("class", "row");
    submitRow.setAttribute("style", "margin: 15px;");
    canvas.appendChild(submitRow);

        let button = document.createElement("button");
        button.setAttribute("type", "submit");
        button.onclick = function() {
            alert("Su repuesta se ha almacenado. \nGracias por participar.")
        }
        button.appendChild(document.createTextNode("Enviar"));
        submitRow.appendChild(button);

})();

function generateOperationPrebody() {
    let prebodyRow = document.createElement("div");
    prebodyRow.setAttribute("class", "row");
    prebodyRow.setAttribute("style", "margin: 15px; width: 1200px");

        let aforadorSupervisor = document.createElement("div");
        aforadorSupervisor.setAttribute("class", "col-sm-2");
        prebodyRow.appendChild(aforadorSupervisor);

            let labelAforador = document.createElement("strong");
            labelAforador.appendChild(document.createTextNode("AFORADOR:  "));
            aforadorSupervisor.appendChild(labelAforador);
            let aforador = document.createElement("input");
            aforador.setAttribute("type", "text");
            aforador.setAttribute("name", "1_1_aforador");
            aforadorSupervisor.appendChild(aforador);

            let labelSupervisor = document.createElement("strong");
            labelSupervisor.appendChild(document.createTextNode("SUPERVISOR:  "));
            aforadorSupervisor.appendChild(labelSupervisor);
            let supervisor = document.createElement("input");
            supervisor.setAttribute("type", "text");
            supervisor.setAttribute("name", "1_2_supervisor")
            aforadorSupervisor.appendChild(supervisor);

        let corredor = document.createElement("div");
        corredor.setAttribute("class", "col-sm-2");
        prebodyRow.appendChild(corredor);

            let labelCorredor = document.createElement("strong");
            labelCorredor.appendChild(document.createTextNode("CORREDOR: "));
            corredor.appendChild(labelCorredor);
            let inputCorredor = document.createElement("input");
            inputCorredor.setAttribute("type", "text");
            inputCorredor.setAttribute("name", "1_3_corredor");
            corredor.appendChild(inputCorredor);

        let entre = document.createElement("div");
        entre.setAttribute("class", "col-sm-2");
        prebodyRow.appendChild(entre);

            let labelEntre = document.createElement("strong");
            labelEntre.appendChild(document.createTextNode("ENTRE: "));
            entre.appendChild(labelEntre);
            let inputEntre = document.createElement("input");
            inputEntre.setAttribute("type", "text");
            inputEntre.setAttribute("name", "1_4_corredor_entre");
            entre.appendChild(inputEntre);

        let dates = document.createElement("div");
        dates.setAttribute("class", "col-sm-4");
        prebodyRow.appendChild(dates);

            let labelFechaToma = document.createElement("strong");
            labelFechaToma.appendChild(document.createTextNode("FECHA TOMA: "));
            dates.appendChild(labelFechaToma);
            let inputFechaToma = document.createElement("input");
            inputFechaToma.setAttribute("type", "date");
            inputFechaToma.setAttribute("name", "1_5_fecha_toma")
            dates.appendChild(inputFechaToma);
            dates.appendChild(document.createElement("br"));

            let labelHoraIni = document.createElement("strong");
            labelHoraIni.appendChild(document.createTextNode("HORA DE INICIO PROGRAMADA: "));
            dates.appendChild(labelHoraIni);
            dates.appendChild(document.createElement("br"));
            let inputHoraIni = document.createElement("input");
            inputHoraIni.setAttribute("type", "time");
            inputHoraIni.setAttribute("name", "1_6_hora_ini_prog")
            dates.appendChild(inputHoraIni);
            dates.appendChild(document.createElement("br"));

            let labelHoraFin = document.createElement("strong");
            labelHoraFin.appendChild(document.createTextNode("HORA DE FINALIZACIÓN PROGRAMADA: "));
            dates.appendChild(labelHoraFin);
            dates.appendChild(document.createElement("br"));
            let inputHoraFin = document.createElement("input");
            inputHoraFin.setAttribute("type", "time");
            inputHoraFin.setAttribute("name", "1_7_hora_fin_prog");
            dates.appendChild(inputHoraFin);

        let toma = document.createElement("div");
        toma.setAttribute("class", "col-sm-2");
        prebodyRow.appendChild(toma);
            let labelTomaIni = document.createElement("strong");
            labelTomaIni.appendChild(document.createTextNode("INICIO TOMA: "));
            toma.appendChild(labelTomaIni);
            toma.appendChild(document.createElement("br"));
            let inputTomaIni = document.createElement("input");
            inputTomaIni.setAttribute("type", "time");
            inputTomaIni.setAttribute("name", "1_8_inicio_toma");
            toma.appendChild(inputTomaIni);
            toma.appendChild(document.createElement("br"));


            let labelTomaFin = document.createElement("strong");
            labelTomaFin.appendChild(document.createTextNode("FIN TOMA: "));
            toma.appendChild(labelTomaFin);
            toma.appendChild(document.createElement("br"));
            let inputTomaFin = document.createElement("input");
            inputTomaFin.setAttribute("type", "time");
            inputTomaFin.setAttribute("name", "1_9_fin_toma");
            toma.appendChild(inputTomaFin);
    return prebodyRow;
}
