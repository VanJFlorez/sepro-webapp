/**
  * Generates the HTML selector that lets the user select the desired type
  * to edit/add to the survey form canvas...
  * This function is called in 'initialization-scripts.js'
  */
(function() {
    let editorHeading = document.getElementById("element_editor_heading");
    let select = document.createElement("select");
    select.setAttribute("name", "type");
    select.setAttribute("class", FORM_EDITOR__GENERIC_VALUE__CLASS);
    select.setAttribute("onclick", "makeBaseElementEditor()");
    editorHeading.appendChild(select);

    let option = document.createElement("option");
    option.appendChild(document.createTextNode("Pregunta Texto Corto"));
    option.setAttribute("value", "1");
    select.appendChild(option);

    option = document.createElement("option");
    option.appendChild(document.createTextNode("Pregunta Valor Numérico"));
    option.setAttribute("value", "2");
    select.appendChild(option);

    option = document.createElement("option");
    option.appendChild(document.createTextNode("Pregunta Abierta"));
    option.setAttribute("value", "3");
    select.appendChild(option);

    option = document.createElement("option");
    option.appendChild(document.createTextNode("Pregunta Selección Multiple"));
    option.setAttribute("value", "4");
    select.appendChild(option);

    option = document.createElement("option");
    option.appendChild(document.createTextNode("Pregunta Selección Unica"));
    option.setAttribute("value", "5");
    select.appendChild(option);

    option = document.createElement("option");
    option.appendChild(document.createTextNode("Tabla"));
    option.setAttribute("value", "6");
    select.appendChild(option);

    option = document.createElement("option");
    option.appendChild(document.createTextNode("Pregunta Si/No"));
    option.setAttribute("value", "7");
    select.appendChild(option);

})();
/**
  * Generates the element editor ui...
  */
function makeBaseElementEditor() {
    editor = document.getElementById("element_editor");
    editor.innerHTML = "";

    text = document.createElement("strong");
    text.appendChild(document.createTextNode("Depende de: "))
    editor.appendChild(text);

    select = document.createElement("select");
    select.setAttribute("name", FORM_EDITOR__DEPENDS_FROM_PREVIOUS_QUESTION__KEY);
    select.setAttribute("class", FORM_EDITOR__GENERIC_VALUE__CLASS);
    editor.appendChild(select);
    editor.appendChild(document.createElement("br"))

    option = document.createElement("option");
    option.appendChild(document.createTextNode(""));
    option.setAttribute("value", "");
    select.appendChild(option);

    for(let i = 0; i < survey.branchQuestionsIds.length; i++) {
        option = document.createElement("option");
        option.appendChild(document.createTextNode(survey.branchQuestionsIds[i]));
        option.setAttribute("value", survey.branchQuestionsIds[i]);
        select.appendChild(option);
    }

    // <label for='QUESTION_KEY'>'TEXT_NODE'<label>
    label = document.createElement("label");
    label.setAttribute("for", QUESTION_KEY);
    label.appendChild(document.createTextNode("¿Cuál es la pregunta?: "))
    editor.appendChild(label);
    editor.appendChild(document.createElement("br"));

    // <input id=QUESTION_KEY class=FORM_EDITOR__GENERIC_VALUE__CLASS name=QUESTION_KEY>
    question = document.createElement("input");
    question.setAttribute("id", QUESTION_KEY);
    question.setAttribute("class", FORM_EDITOR__GENERIC_VALUE__CLASS);
    question.setAttribute("name", QUESTION_KEY);
    editor.appendChild(question);
    editor.appendChild(document.createElement("br"));

    buildElementEditor(editor);

    // <button onclick="ensamble()">
    editor.appendChild(document.createElement("br"));
    addButton = document.createElement("button");
    addButton.appendChild(document.createTextNode("Añadir a la encuesta."));
    addButton.setAttribute("onclick", "ensamble()");
    editor.appendChild(addButton);
}
/**
  * Builds the specific element editor... (input text editor, table editor, selection editor)
  * @param {Node} node - HTML DOM node where element editor will be appended
*/
function buildElementEditor(node) {
    values = getValues();
    type = Number(values.get(TYPE_KEY));

    switch (type) {
        case 1: // input text
            // actually do not requires especial fields in base editor
            break;
        case 2: // input number
            // actually do not requires especial fields in base editor
            break;
        case 3: // text textarea
            // actually do not requires especial fields in base editor
            break;
        case 4:
            inputEditorSelection(node);
            break;
        case 5:
            inputEditorSelection(node);
            break;
        case 6:
            inputEditorTable(node);
            break;
        case 7:
            inputEditorYesNo(node);
            break;
        default:
            console.log("bad option....")
            break;
    }
}
/**
    input := node where search begin
    return := key value map that contains data provided by user in "element_editor" div.
*/
function getValues() {
    values = document.getElementsByClassName(FORM_EDITOR__GENERIC_VALUE__CLASS); // This can be optimized if the search begining in "element_editor" node
    options = document.getElementsByClassName(SELECTION_EDITOR__OPTION__CLASS);
    kv = new Map(); // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
    opt = [];
    for(i = 0; i < values.length; i++)
        kv.set(values[i].name, values[i].value);

    for(i = 0; i < options.length; i++)
        opt.push(options[i].value);
    kv.set(SELECTION_EDITOR__OPTIONS__KEY, opt);

    return kv;
}
/**
 * Retrieves values from editor. Note that the values are retrieved based on the
 * id of the input form that holds the value. This id are written in capitalized
 * characters and are global variables defined in 'initialization-scripts.js' file.
 * @returns {Map} - A Map holding all values from the editor's fields. They can
 *                  retrieved with the global var constants defined in
 *                  'initialization-scripts.js' file. See it for details...
 */
function getValuesFromEditor() {
    values = document.getElementsByClassName(FORM_EDITOR__GENERIC_VALUE__CLASS); // This can be optimized if the search begining in "element_editor" node
    options = document.getElementsByClassName(SELECTION_EDITOR__OPTION__CLASS);
    optionsHasText = document.getElementsByClassName(SELECTION_EDITOR__OPTION_HAS_TEXT__CLASS);

    let kv = new Map(); // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
    let opt = [];
    // adding all generic values to map
    for(let i = 0; i < values.length; i++)
        kv.set(values[i].name, values[i].value);

    for(let i = 0; i < options.length; i++)
        opt.push([options[i].value, optionsHasText[i].value]); // [string that will appear next to the radio/checkbox, "on" = false or "true" = true]
    kv.set(SELECTION_EDITOR__OPTIONS__KEY, opt);

    // retrieves all header strings
    headers = document.getElementsByClassName(HEADER_STRINGS_CLASS);
    let temp = [];
    for(let i = 0; i < headers.length; i++) {
        try {
            temp.push(headers[i].value);
        } catch(err) {
             // console.log(err);
        }
    }
    kv.set(TABLE_EDITOR__HEADERS_KEY, temp);

    // retrieves column types
    let cols = kv.get(TABLE_EDITOR__COLUMNS_QUANTITY_KEY);
    temp = [];
    for(let i = 0; i < cols; i++) {
        try {
            temp.push(document.getElementById(COLUMN_TYPE_KEY + "_" + i).value);
        } catch(err) {
            // console.log(err.message);
        }
    }
    kv.set(TABLE_EDITOR__COLUMN_TYPES_LIST_KEY, temp);

    // Gets options from subeditor
    temp = [];
    let strings;
    for(let i = 0; i < cols; i++) {
        options = document.getElementsByClassName(TABLE_EDITOR__SUBEDITOR__OPTIONS_CLASS_PREFIX + "_" + i);
        strings = [];
        for(let j = 0; j < options.length; j++)
            strings.push(options[j].value);
        temp.push(strings);
    }
    kv.set(TABLE_EDITOR__SUBEDITOR__OPTIONS_CLASS_PREFIX, temp);
    return kv;
}
