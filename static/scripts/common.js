/**
  Params:
    id := the id of the row, used when is removed from DOM. In html has the form = row_<id>
    label := this label will hold the question that groups this row.
  Returns a div where is valid append childs...

  IMPORTANT:
    The remove logic is found in onclick method defined here...
*/
function makeRow(id, textLabel) {
    // form_canvas ~> divRow
    divRow = document.createElement("div");
    divRow.setAttribute("id", "row_" + id)
    divRow.setAttribute("class", "row");
    document.getElementById("form_canvas").appendChild(divRow);
    // form_canvas ~> divRow ~> divCard
    divCard = document.createElement("div");
    divCard.setAttribute("class", "panel panel-default card col-sm-8");
    divRow.appendChild(divCard);
    // form_canvas ~> divRow ~> divCard ~> divCardContent
    divCardContent = document.createElement("div");
    divCardContent.setAttribute("class", "panel-body card-content");
    divCard.appendChild(divCardContent);
    // form_canvas ~> divRow ~> divCard ~> divCardContent ~> label
    label = document.createElement("label");
    label.setAttribute("for", id);
    label.appendChild(document.createTextNode(id + ". "));
    label.appendChild(document.createTextNode(textLabel))
    divCardContent.appendChild(label);
    divCardContent.appendChild(document.createElement("br"));
    /**
        // Functionality virtually goes here.
    */
    // form_canvas ~> divRow ~> divBtn
    divBtn = document.createElement("div");
    divBtn.setAttribute("class", "col-sm-4");
    divRow.appendChild(divBtn);
    // form_canvas ~> divRow ~> divBtn ~> btn
    btn = document.createElement("button");
    btn.setAttribute("name", "delete");
    btn.appendChild(document.createTextNode("eliminar!"));
    btn.onclick = function() {
        // var row = document.getElementById("row_" + id);
        // document.getElementById("form_canvas").removeChild(row);
        survey.deleteFormElement(id - 1);  // survey.form_index = formElement.id - 1
                                         // This deletion method works only if each row updates
                                         // its id after deletion of other row. So, After
                                         // deletion is important to call drawFromList(...) method.
        drawFromList(); // recomputes all form elements ID's IMPORTANTISIMO!!!!!!!!!!!!!!!!!
    }
    divBtn.appendChild(btn);
    divBtn.appendChild(document.createElement("br"));


/*
    let input = document.createElement("input");
    input.setAttribute("type", "number");
    input.setAttribute("min", "1");
    input.setAttribute("class", FORM_EDITOR__GENERIC_VALUE__CLASS);
    input.setAttribute("name", YESNO_EDITOR__DEPENDANT_QUESTIONS__KEY);
    divBtn.appendChild(input);
*/
    return divCardContent;
}
/**
  If passed array is composed of FormElement Instances, the next function
  draws the entire form....
*/
function drawFromList() {
  canvas = document.getElementById("form_canvas");
  while(canvas.hasChildNodes()) // Clean up canvas before draw. Can be faster remove canvas an creating new one
    canvas.removeChild(canvas.lastChild)
  for(let i = 0; i < survey.form.length; i++) {
    body = makeRow(i + 1, survey.form[i].mainQuestion);
    canvas.appendChild(body.parentNode.parentNode); // card-content <~ card <~ row <~ body
    formElement = survey.form[i].element;
    body.appendChild(formElement);
  }
  return canvas;
}
/**
  * Written by: https://stackoverflow.com/users/949476/dfsq
  * Makes that the innerHTML of the passed node will be indented.
  */
function format(node, level) {
    var indentBefore = new Array(level++ + 1).join('  '),
      indentAfter  = new Array(level - 1).join('  '),
      textNode;

    for (var i = 0; i < node.children.length; i++) {

      textNode = document.createTextNode('\n' + indentBefore);
      node.insertBefore(textNode, node.children[i]);

      format(node.children[i], level);

      if (node.lastElementChild == node.children[i]) {
          textNode = document.createTextNode('\n' + indentAfter);
          node.appendChild(textNode);
      }
    }

    return node;
}
/**
 * Converts the current DOM tree in HTML and indents its content...
 */
document.getElementById("save_button").onclick = function() {
    let formSource = document.getElementById("form_canvas"); //.cloneNode(true);
    if (formSource == null) // it seems that we are making a format
        formSource = document.getElementById("format_canvas");

    let head = document.getElementsByTagName("head")[0];
    let indentedHead = format(head, 0);

    let indentedHTMLSource = format(formSource, 0);
    // let htmlFile = new Blob(["<!DOCTYPE html> <pre><code>" + formSource.innerHTML + "</pre></code>"], {type: 'text/html'}); // change text/html <~> text/raw to display or not doc or text
    let htmlFile = new Blob(["<!DOCTYPE html>\n" +
                             "<head>\n" +
                                indentedHead.innerHTML +
                             "</head>\n" +
                             "<body>\n" +
                                indentedHTMLSource.innerHTML +
                             "</body>\n"], {type: 'text/raw'}); // change text/html <~> text/raw to display or not doc or text
    let url = window.URL.createObjectURL(htmlFile)

    a = document.getElementById("source_link");
    a.removeAttribute("hidden");
    a.setAttribute("href", url);

    /*
    if(confirm("desea descargar archivos adicionales?")) {
    // download such files bootstrap jquery
    } else {
    // do nothing
    }
    */
}
/**
    common function to build every element form object
*/
function ensamble() {
    let values = getValuesFromEditor();
    let type = Number(values.get(TYPE_KEY));
    switch (type) {
        case 1:
            buildInputText(1);
            break;
        case 2:
            buildInputText(2);
            break;
        case 3:
            buildInputText(3);
            break;
        case 4:
            buildSelection(2);
            break;
        case 5:
            buildSelection(1);
            break;
        case 6:
            buildTable();
            break;
        case 7:
            buildYesNo();
            break;
        default:
            console.log("bad option...");
    }
}
/**
 *  This function retrieves the dependant yesno question from a given question.
 *  @param {Map} values - The current set of values from editor that was retrieved in build<elementType>()
 *                        step.
 *  @return {FormElement} deQuestionRef - A reference to dependant yesno question.
 */
 function getDependantQuestion(values) {
     let depQuestionNum = Number(values.get(FORM_EDITOR__DEPENDS_FROM_PREVIOUS_QUESTION__KEY))
     let depQuestionRef = null;
     if(depQuestionNum != 0)
        depQuestionRef = survey.form[depQuestionNum - 1];
    return depQuestionRef;
 }
