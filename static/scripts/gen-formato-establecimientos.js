// Formato Inventario Infraestructura y Establecimientos (1 de 2)

(function genColumnLabeledInput() {
    let canvas = document.getElementById("format_canvas");
    let trans = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    let columnQuantity = 2; // prompt();
    let rowQuantity = 38; // prompt();

    // head -------------------------------------------------------------------
    let header = generateGenericFormatHeader("FORMATO DE INVENTARIO DE ESTABLECIMIENTOS", "CARACTERIZACIÓN Y ANÁLISIS DEL CARGUE Y DESCARGUE DE MERCANCÍAS EN EL CENTRO DE LA CIUDAD DE NEIVA PARA LA IDENTIFICACIÓN DE ALTERNATIVAS PARA MEJORAR LA MOVILIDAD");
    canvas.appendChild(header);

    // pre body ---------------------------------------------------------------
    let prebody = document.createElement("div");
    prebody.setAttribute("class", "row");
    prebody.setAttribute("style", "margin-top: 15px");
    canvas.appendChild(prebody);

    prebody.appendChild(document.createTextNode("Tramo"));
    let tramo = document.createElement("input");
    tramo.setAttribute("type", "text");
    tramo.setAttribute("name", "1_1_tramo")
    tramo.setAttribute("style", "margin: 30px");
    prebody.appendChild(tramo);

    prebody.appendChild(document.createTextNode("entre"));
    let entre = document.createElement("input");
    entre.setAttribute("type", "text");
    entre.setAttribute("name", "1_2_inicio")
    entre.setAttribute("style", "margin: 30px");
    prebody.appendChild(entre);

    prebody.appendChild(document.createTextNode("y"));
    let and = document.createElement("input");
    and.setAttribute("type", "text");
    and.setAttribute("name", "1_3_fin");
    and.setAttribute("style", "margin: 30px");
    prebody.appendChild(and);

    prebody.appendChild(document.createTextNode("fecha"));
    let date2 = document.createElement("input");
    date2.setAttribute("type", "date");
    date2.setAttribute("name", "1_4_fecha");
    // date.setAttribute("style", "margin: 30px");
    prebody.appendChild(date2);


    // body -------------------------------------------------------------------
    let outerLimit = document.createElement("div");
    outerLimit.setAttribute("style", "border:1px solid black; margin-top: 15px; text-align: center; ");
    outerLimit.setAttribute("class", "row");
    canvas.appendChild(outerLimit);

    let title = document.createElement("div");
    title.setAttribute("style", "border-bottom: 1px solid black; margin-bottom: 30px; font-weight: bold;");
    title.appendChild(document.createTextNode("INVENTARIO DE ESTABLECIMIENTOS"));
    outerLimit.appendChild(title);

    let limite = document.createElement("div"); // div that holds column divs
    limite.setAttribute("class", "row");
    limite.setAttribute("style", "margin: 5px;"); // Fix: no colorea!
    outerLimit.appendChild(limite);

    for(let i = 0; i < columnQuantity; i++) {
        let colDiv = document.createElement("div");
        colDiv.setAttribute("class", "col-sm-6")
        limite.appendChild(colDiv);
        let table = document.createElement("table"); // using table guarraties the format of elements...
        table.setAttribute("width", "80%")
        if (i%2 == 1)
            table.setAttribute("align", "right")
        colDiv.appendChild(table);

        for(let j = 0; j < rowQuantity; j++) {
            let row = document.createElement("tr");
            table.appendChild(row);

            let name = (j + 1) + trans[i];
            let labelText = (j + 1) + trans[i];

            let label = document.createElement("strong");
            label.appendChild(document.createTextNode(labelText));

            let tdLabel = document.createElement("td");
            tdLabel.appendChild(label);
            row.appendChild(tdLabel);

            let input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("name", name);
            input.setAttribute("style", "border: 0; width: 100%;");

            let tdInput = document.createElement("td");
            tdInput.appendChild(input);
            row.appendChild(tdInput);
        }
    }

    // footer -----------------------------------------------------------------
    let footDiv = document.createElement("div");
    footDiv.setAttribute("style", "border:1px solid black; margin-top: 15px;");
    footDiv.setAttribute("class", "row");
    canvas.appendChild(footDiv);

    let dummyDiv = document.createElement("div");
    dummyDiv.setAttribute("class", "col-sm-1");
    footDiv.appendChild(dummyDiv);

    let aforadorWrapper = document.createElement("div");
    aforadorWrapper.setAttribute("class", "col-sm-4");
    aforadorWrapper.setAttribute("style", "padding: 0%; margin: 5px");
    footDiv.appendChild(aforadorWrapper);
    let aforador = document.createElement("div");
    aforador.setAttribute("style", "border: 1px solid black;")
    aforadorWrapper.appendChild(aforador);
    let inputAforador = document.createElement("textarea");
    inputAforador.setAttribute("name", "3_1_aforador");
    inputAforador.setAttribute("style", "border: 0; height: 60px; width: 100%;")
    inputAforador.setAttribute("placeholder", "AFORADOR:")
    aforador.appendChild(inputAforador);

    let supervisorWrapper = document.createElement("div");
    supervisorWrapper.setAttribute("class", "col-sm-4");
    supervisorWrapper.setAttribute("style", "padding: 0%; margin: 5px;");
    footDiv.appendChild(supervisorWrapper);
    let supervisor = document.createElement("div");
    supervisor.setAttribute("style", "border:1px solid black;");
    supervisorWrapper.appendChild(supervisor);
    let inputSupervisor = document.createElement("textarea");
    inputSupervisor.setAttribute("name", "3_2_supervisor");
    inputSupervisor.setAttribute("style", "border: 0; height: 60px; width: 100%;")
    inputSupervisor.setAttribute("placeholder", "SUPERVISOR:")
    supervisor.appendChild(inputSupervisor);

    let upzWrapper = document.createElement("div");
    upzWrapper.setAttribute("class", "col-sm-2");
    upzWrapper.setAttribute("style", "padding: 0%; margin: 5px;")
    footDiv.appendChild(upzWrapper);
    let upz = document.createElement("div");
    upz.setAttribute("style", "border:1px solid black;");
    upzWrapper.appendChild(upz);
    let inputUpz = document.createElement("textarea");
    inputUpz.setAttribute("name", "3_3_UPZ");
    inputUpz.setAttribute("style", "border: 0; height: 60px; width: 100%;")
    inputUpz.setAttribute("placeholder", "UPZ:");
    upz.appendChild(inputUpz);

    // submit button ----------------------------------------------------------
    let submitRow = document.createElement("div");
    submitRow.setAttribute("class", "row");
    submitRow.setAttribute("style", "margin: 15px;");
    canvas.appendChild(submitRow);

    let button = document.createElement("button");
    button.setAttribute("type", "submit");
    button.onclick = function() {
        alert("Su repuesta se ha almacenado. \nContinúe porfavor con la siguiente parte.")
    }
    button.appendChild(document.createTextNode("Enviar"));
    submitRow.appendChild(button);

})();
