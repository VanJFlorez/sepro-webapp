/**
 * Builds in the build_form_gui.html file the ui for create/edit  tables...
 * @param {Node} node - Node where the editor DOM elements will be appended
 */
function inputEditorTable(node) {
    // <label for=TABLE_EDITOR__COLUMNS_QUANTITY_KEY>¿Cuantas columnas?</label>
    label = document.createElement("label");
    label.setAttribute("for", TABLE_EDITOR__COLUMNS_QUANTITY_KEY);
    label.appendChild(document.createTextNode("¿Cuantas columnas?: "));
    node.appendChild(label);
    node.appendChild(document.createElement("br"));
    // <input type=number id=TABLE_EDITOR__COLUMNS_QUANTITY_KEY class=
    columns = document.createElement("input");
    columns.setAttribute("type", "number");
    columns.setAttribute("id", TABLE_EDITOR__COLUMNS_QUANTITY_KEY); // required for label
    columns.setAttribute("class", FORM_EDITOR__GENERIC_VALUE__CLASS); // for retrieving purposes step 1
    columns.setAttribute("name", TABLE_EDITOR__COLUMNS_QUANTITY_KEY); // for retrieving purposes step 2
    columns.setAttribute("onchange", "addHeadersToEditor( )")
    node.appendChild(columns);
    node.appendChild(document.createElement("br"));

    label = document.createElement("label");
    label.setAttribute("for", TABLE_EDITOR__ROWS_QUANTITY_KEY);
    label.appendChild(document.createTextNode("¿Cuantas filas?: "));
    node.appendChild(label);
    node.appendChild(document.createElement("br"));

    rows = document.createElement("input");
    rows.setAttribute("type", "number");
    rows.setAttribute("id", TABLE_EDITOR__ROWS_QUANTITY_KEY);
    rows.setAttribute("class", FORM_EDITOR__GENERIC_VALUE__CLASS);
    rows.setAttribute("name", TABLE_EDITOR__ROWS_QUANTITY_KEY);
    node.appendChild(rows);
    node.appendChild(document.createElement("hr"));

    h5 = document.createElement("strong");
    h5.appendChild(document.createTextNode("Editar columna: "))
    node.appendChild(h5);

    // header input form are added here....
    headersNode = document.createElement("div");
    headersNode.setAttribute("id", HEADERS_NODE_ID);
    node.appendChild(headersNode);
}
/**
 * This function is only called by inputEditorTable(node) whenever number of
 * columns is changed. Draws input form elements for each column defined by the
 * user.
 */
function addHeadersToEditor() {
    columns = Number(getValuesFromEditor().get(TABLE_EDITOR__COLUMNS_QUANTITY_KEY));

    TABLE_EDITOR__SUBEDITOR_COUNTERS = [columns]; // GLOBAL VARIABLE
    TABLE_EDITOR__NODES_TO_APPEND_OPTIONS = [columns]; // GLOBAL VARIABLE
    TABLE_EDITOR__NODES_SUBEDITOR_REFERENCES = [columns]; // GLOBAL VARIABLE

    headersNode = document.getElementById(HEADERS_NODE_ID);
    headersNode.innerHTML = "";

    tabs = document.createElement("ul");
    tabs.setAttribute("class", "nav nav-tabs");
    headersNode.appendChild(tabs);

    tabContent = document.createElement("div");
    tabContent.setAttribute("class", "tab-content");
    headersNode.appendChild(tabContent);

    for(let i = 0; i < columns; i++) { // i actually denotes the column number, and its used to setup all associated ids for its subeditor id
        // Tab decoration
        li = document.createElement("li");
        if(i == 0)
            li.setAttribute("class", "active");
        a = document.createElement("a");
        a.setAttribute("href", "#" + HEADERS_NODE_ID + "_" + i);
        a.setAttribute("data-toggle", "tab");
        a.appendChild(document.createTextNode("" + (i + 1))); // tab title
        li.appendChild(a);
        tabs.appendChild(li);

        // Tab content
        divTab = document.createElement("div");
        divTab.setAttribute("id", HEADERS_NODE_ID + "_" + i);
        divTab.setAttribute("class", "tab-pane fade");
        if(i == 0)
            divTab.setAttribute("class", "tab-pane fade in active");
        divTab.appendChild(document.createElement("br"));
        tabContent.appendChild(divTab); // Append now in order to appear in the tag searches from the following lines

            // label for ask title
            label = document.createElement("label");
            label.setAttribute("for", HEADER_STRINGS_CLASS + "_" + i);
            label.appendChild(document.createTextNode("¿Título para la columna?: "));
            divTab.appendChild(label);
            divTab.appendChild(document.createElement("br"));

            // input for get the title
            title = document.createElement("input");
            title.setAttribute("type", "text");
            title.setAttribute("id", HEADER_STRINGS_CLASS + "_" + i);
            title.setAttribute("class", HEADER_STRINGS_CLASS);
            title.setAttribute("name", HEADER_STRINGS_CLASS);
            divTab.appendChild(title);
            divTab.appendChild(document.createElement("br"));
            divTab.appendChild(document.createElement("br"));

            // text that sumarizes the available types
            text = document.createElement("pre");
            text.appendChild(document.createTextNode("Tipo disponible para la columna " + (i + 1) + " :" +
                                                        "\n1. Texto simple" +
                                                        "\n2. Selección multiple" +
                                                        "\n3. Selección única" +
                                                        "\n4. Si/No"));
            divTab.appendChild(text);
            divTab.appendChild(document.createElement("br"));

            // input to get the desired type
            input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("id", COLUMN_TYPE_KEY + "_" + i);
            input.setAttribute("name", COLUMN_TYPE_KEY + "_" + i);
            input.setAttribute("value", "1");
            input.setAttribute("max", "4");
            input.setAttribute("min", "1");
            input.setAttribute("class", FORM_EDITOR__GENERIC_VALUE__CLASS);
            input.setAttribute("onchange", "inputEditorTableColumnTypeEditor(" + i + ")" )
            divTab.appendChild(input);
            divTab.appendChild(document.createElement("br"));
            divTab.appendChild(document.createElement("br"));

            // draws subeditor for choosed column type
            divSubEditor = document.createElement("div");
            divSubEditor.setAttribute("id", "append_subeditor_here");
            divTab.appendChild(divSubEditor);

            TABLE_EDITOR__NODES_SUBEDITOR_REFERENCES[i] = divSubEditor;

            inputEditorTableColumnTypeEditor(i);
    }
}
/**
 *
 */
function inputEditorTableColumnTypeEditor(columnNumber) {
    divSubEditor = TABLE_EDITOR__NODES_SUBEDITOR_REFERENCES[columnNumber];
    divSubEditor.innerHTML = ""; // as this function is called whenever type changes, we clean up the subEditor canvas first.

    let type = Number(getValuesFromEditor().get(COLUMN_TYPE_KEY + "_" + columnNumber));
    switch (type) {
      case 1:
            input = document.createElement("input");
            input.setAttribute("disabled", "");
            input.setAttribute("placeholder", "Texto simple");
            divSubEditor.appendChild(input);
            break;
      case 2:
            title = document.createElement("strong");
            title.appendChild(document.createTextNode("Cantidad de opciones: "));
            divSubEditor.appendChild(title);
            divSubEditor.appendChild(document.createElement("br"));

            input = document.createElement("input");
            input.setAttribute("type", "number");
            input.setAttribute("min", "1");
            input.setAttribute("name", TABLE_EDITOR__SUBEDITOR__NUMBER_OF_OPTIONS_KEY_PREFIX + "_" + columnNumber);
            input.setAttribute("onchange", "addSubEditorOptions(" + columnNumber + ")"); // currently, I do not know how to pass references here, so columnNumber plays this role....
            input.setAttribute("class", FORM_EDITOR__GENERIC_VALUE__CLASS);
            divSubEditor.appendChild(input);

            divOptions = document.createElement("div");
            divOptions.setAttribute("id", "append_subeditor_selection_options_here" + "_" + columnNumber);
            divSubEditor.appendChild(divOptions);

            TABLE_EDITOR__NODES_TO_APPEND_OPTIONS[columnNumber] = divOptions;

        break;
      case 3:
        numOptions = prompt("cuántas opciones? (número)");
        options = [];
        for(let i = 0; i < numOptions; i++) {
          option = prompt("opción #" + (i + 1) + ": ");
          options.push(option);
        }
        instance = new SelectionElement(2, -1, "", options); // This input will be overrided serveral times in createTable(...)
        break;
      case 4:
        instance = new SelectionYesNo(-1)
        break;
      default:
        console.log("This option is not implemented yet...");
    }
}

function addSubEditorOptions(columnNumber) {
    let optionsNumber = getValuesFromEditor().get(TABLE_EDITOR__SUBEDITOR__NUMBER_OF_OPTIONS_KEY_PREFIX + "_" + columnNumber);
    let divOptions = TABLE_EDITOR__NODES_TO_APPEND_OPTIONS[columnNumber];
    divOptions.innerHTML = ""; // as this function is called whenever number of options changed, we clean this canvas first...
    for(let i = 0; i < optionsNumber; i++) {
        label = document.createElement("label");
        label.setAttribute("for", TABLE_EDITOR__SUBEDITOR__OPTIONS_CLASS_PREFIX + "_" + columnNumber + "_" + i);
        label.appendChild(document.createTextNode("Opción #" + (i + 1) + ":"));
        divOptions.appendChild(label);
        divOptions.appendChild(document.createElement("br"));

        option = document.createElement("input");
        option.setAttribute("class", TABLE_EDITOR__SUBEDITOR__OPTIONS_CLASS_PREFIX + "_" + columnNumber);  // lets you retrieve all options in a list.... see getValues()
        option.setAttribute("id", TABLE_EDITOR__SUBEDITOR__OPTIONS_CLASS_PREFIX + "_" + columnNumber + "_" + i); // id used only by label tag
        divOptions.appendChild(option);
        divOptions.appendChild(document.createElement("br"));
    }
}

/**
 * This function sumarizes three main processes, first creates the row where the
 * form element will be placed, after that retrieves the values from the element
 * editor, and then creates the associated element.
 */
function buildTable() {
   CURRENT_ID = CURRENT_ID + 1;
    let values = getValuesFromEditor();
    let textLabel = values.get(QUESTION_KEY);

    let cols = values.get(TABLE_EDITOR__COLUMNS_QUANTITY_KEY);
    let rows = values.get(TABLE_EDITOR__ROWS_QUANTITY_KEY);

    let headers = values.get(TABLE_EDITOR__HEADERS_KEY);
    let colTypes = values.get(TABLE_EDITOR__COLUMN_TYPES_LIST_KEY);

    let depQuestionRef = getDependantQuestion(values);

    let colTypeInstances = [];
    for(let i = 0; i < colTypes.length; i++) { // read i as columnNumber
        let instance = null;
        switch (Number(colTypes[i])) {
          case 1:
            instance = new InputTextElement(1, -1, ""); // This input will be overrided serveral times in createTable(...)
            break;
          case 2:
            options = values.get(TABLE_EDITOR__SUBEDITOR__OPTIONS_CLASS_PREFIX)[i];
            /**
             * as Selection element reads an array of array as options params,
             * we add a dummy string to convert to that form... see gen-selection.js~>createSelection(...)
             * and element-editor.js~>getValuesFromEditor(...) for details
             */
            fixOptions = [];
            for(let i = 0; i < options.length; i++)
                fixOptions.push([options[i], "on"]);
            instance = new SelectionElement(1, -1, "", fixOptions, null); // This input will be overrided serveral times in createTable(...)
            break;
          case 3:
            numOptions = prompt("cuántas opciones? (número)");
            options = [];
            for(let i = 0; i < numOptions; i++) {
              option = prompt("opción #" + (i + 1) + ": ");
              options.push(option);
            }
            instance = new SelectionElement(2, -1, "", options, null); // This input will be overrided serveral times in createTable(...)
            break;
          case 4:
            instance = new SelectionYesNo(-1)
            break;
          default:
            console.log("This option is not implemented yet...");
            instance = null;
        }
        colTypeInstances.push(instance);
    }
    let body = makeRow(CURRENT_ID, textLabel);
    let table = new TableElement(CURRENT_ID, textLabel, cols, rows, colTypeInstances, headers, depQuestionRef);
    body.appendChild(table.element);
    survey.addFormElement(table);
}
/**
 * Return an <table></table> HTML DOM element...
 */
function createTable(baseId, cols, rows, colTypeInstances, headers, depQuestionRef) {
    // here we ask to user the type of each column
    let id = baseId;
    let formElement = document.createElement("div");
    let table = document.createElement("table");

    if(depQuestionRef != null)
        formElement.setAttribute("class", depQuestionRef.id + "_derived_hide");

    for(var i = 0; i < rows; i++) {
        tr = document.createElement("tr"); // creates a table row
        table.appendChild(tr);
        for(var j = 0; j < cols; j++) {
            if(i == 0) {
                th = document.createElement("th"); // create a table header for each column for first row
                tr.appendChild(th);
                th.appendChild(document.createTextNode(headers[j]));
            } else {
                td = document.createElement("td"); // create a table entry for each column  for each row
                tr.appendChild(td);
                var input = null;
                var finalId = baseId + "_" + i + "_" + j;
                colTypeInstances[j].updateId(finalId)
                td.appendChild(colTypeInstances[j].element);
            }
        }
    }

    formElement.appendChild(table);
    return formElement;
}
