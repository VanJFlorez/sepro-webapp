/**
    appends a new row to form_canvas and inside it creates an input element
*/
function buildInputText(type) {
   CURRENT_ID = CURRENT_ID + 1;

    values = getValuesFromEditor();

    let textLabel = values.get(QUESTION_KEY);
    let depQuestionRef = getDependantQuestion(values);

    let body = makeRow(CURRENT_ID, textLabel);

    let inputTextElement = null;
    switch (type) {
      case 1:
        inputTextElement = new InputTextElement(1, CURRENT_ID, textLabel, depQuestionRef);
        break;
      case 2:
        inputTextElement = new InputTextElement(2, CURRENT_ID, textLabel, depQuestionRef);
        break;
      case 3:
        inputTextElement = new InputTextElement(3, CURRENT_ID, textLabel, depQuestionRef);
        break;
      default:
        console.log("This option is not implemented yet!!!")
    }
    body.appendChild(inputTextElement.element); // updates DOM tree (i.e. the html document)
    survey.addFormElement(inputTextElement);    // updates the data structure that tracks every form element
}
/**
  return a <input> HTML DOM element
  Only associated FormElement object interacts with this function...
*/
function createInputText(type, baseId, depQuestionRef) {
    let id = baseId;
    // form_canvas ~> divRow ~> divCard ~> divCardContent ~> input
    let input = null;
    let formElement = document.createElement("div"); // used as enclosing element...

    if(depQuestionRef != null)
        formElement.setAttribute("class", depQuestionRef.id + "_derived_hide");

    switch (type) {
      case 1:
        input = document.createElement("input");
        input.setAttribute("type", "text");
        input.setAttribute("id", id);
        input.setAttribute("name", id);
        break;
      case 2:
        input = document.createElement("input");
        input.setAttribute("type", "number");
        input.setAttribute("id", id);
        input.setAttribute("name", id);
        break;
      case 3:
        input = document.createElement("textarea");
        input.setAttribute("id", id);
        input.setAttribute("name", id);
        input.setAttribute("rows", "3");
        input.setAttribute("cols", "50");
        break;
      default:
        console.log("This option is not implemented yet...");
      }

      formElement.appendChild(input);
      return formElement;
}
