function hide(element) {
    if(element.checked == true) {
        document.getElementById(element.id + "_1").style.display = "block";
        document.getElementById(element.id + "_2").style.display = "block";
    } else {
        let field;
        console.log(element.id);
        field = document.getElementById(element.id + "_1").style.display = "none";
        field.value = "";
        fiel = document.getElementById(element.id + "_2").style.display = "none";
        field.value = "";
    }
}
// for legacy purposes Not used...
function enable(option){
    if (option.checked == true){
        document.getElementById(option.id+"_text").style.display = "block";
    } else {
        document.getElementById(option.id+"_text").style.display = "none";
        document.getElementById(option.id+"_text_v").value = "";
    }
}
// uses option.inner-id instead of option.id
function enableV2(option){
    if (option.checked == true){
        try {
            document.getElementById(option.getAttribute("inner-id") + "_text").style.display = "block";
        } catch(err) {
            console.log("Esta opción no tiene una entrada de texto asociada.")
        }
    } else {
        document.getElementById(option.getAttribute("inner-id") + "_text").style.display = "none";
        document.getElementById(option.getAttribute("inner-id") + "_text_v").value = "";
    }
}

function enable_radio(option){
    if (option.getAttribute("data-a") == "expand"){
        document.getElementById(option.id+"_text").style.display = "block";
    } else {
        document.getElementById(option.id+"_text").style.display = "none";
        document.getElementById(option.id+"_text_v").value = "";
    }
}


function radio_dependences(option) {
    if (option.getAttribute("data-a") == "expand") {
        var hide = document.getElementsByClassName(option.getAttribute("questionNumber") + "_derived_hide");
        for (a = 0; a < hide.length; a++)
            hide[a].style.display = "block";
    } else {
        var hide = document.getElementsByClassName(option.getAttribute("questionNumber") + "_derived_hide");
        for (a = 0; a < hide.length; a++)
            hide[a].style.display = "none";
    }
}
