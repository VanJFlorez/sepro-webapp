/**
  in survey we will hold form elements that will commited finally to the server
  when user finishes edits...
*/
var survey = new Survey();
/**
  Important:
    The following variable CURRENT_ID is global and denotes two important things:
    1. The number of the question
    2. The base name of the form's submited data. What means 'base name'?
       If multiple values are submited for one question the name of the associated
       value has the following form: id_<integer number>
    The value of id is updated in two cases:
    1. When form element is created. (see each build<Element>(...) function)
    2. When delete button is pressed on the guit (see makeRow(...) function)
    Note: the build functions first update the available id...
*/
var CURRENT_ID = 0; // GLOBAL VARIABLE
var currentColumnNumber = 0; // Used for communication between functions

var ATTRIBUTE_TYPE_NAME = "attribute_type_name" // This is the name of additonal attribute in DOM elements that will contain the name of the asociated element in our convention, see following lines
    var ATTRIBUTE_TYPE_NAME__YES_NO_QUESTION = "yesno_question";

/**
    The retrieving of variables is made in two passes:
     1 - first get all fields that belongs to FORM_EDITOR__GENERIC_VALUE__CLASS:
     2 - ther retrieve by its <VALUE>_KEY id....
*/
var FORM_EDITOR__GENERIC_VALUE__CLASS = "FORM_EDITOR__GENERIC_VALUE__CLASS";   // To retrieve unique values
var FORM_EDITOR__DEPENDS_FROM_PREVIOUS_QUESTION__KEY = "FORM_EDITOR__DEPENDS_FROM_PREVIOUS_QUESTION__KEY";

var QUESTION_KEY = "question_id";
var TYPE_KEY = "type";

// for table
var TABLE_EDITOR__SUBEDITOR_COUNTERS = []; // used in addHeadersToEditor(); ACTUALLY NOT IN USE
var TABLE_EDITOR__NODES_TO_APPEND_OPTIONS = []; // used as binding between inputEditorTableColumnTypeEditor() and addSubEditorOption(number) in the editor of tables in addHeadersToEditor();
var TABLE_EDITOR__NODES_SUBEDITOR_REFERENCES = [] // used in addHeadersToEditor();

var TABLE_EDITOR__COLUMNS_QUANTITY_KEY = "TABLE_EDITOR__COLUMNS_QUANTITY_KEY";               // unique
var TABLE_EDITOR__ROWS_QUANTITY_KEY = "TABLE_EDITOR__ROWS_QUANTITY_KEY";                     // unique

var HEADER_STRINGS_CLASS = "header_string_class";   // list retrieves all header or table  titles...
var TABLE_EDITOR__HEADERS_KEY = "TABLE_EDITOR__HEADERS_KEY";

var COLUMN_TYPE_KEY = "column_type_key";            // unique
var TABLE_EDITOR__COLUMN_TYPES_LIST_KEY = "TABLE_EDITOR__COLUMN_TYPES_LIST_KEY";

var COLUMN_TYPE_KEY_BASE = "column_type_key_"       // list, fetches options from column editor
var TABLE_EDITOR__SUBEDITOR__NUMBER_OF_OPTIONS_KEY_PREFIX = "TABLE_EDITOR__SUBEDITOR__NUMBER_OF_OPTIONS_KEY_PREFIX";
var TABLE_EDITOR__SUBEDITOR__OPTIONS_CLASS_PREFIX = "TABLE_EDITOR__SUBEDITOR__OPTIONS_CLASS_PREFIX"
var COLUMN_TYPE_SUBEDITOR_OPTIONS_CLASS = "COLUMN_TYPE_SUBEDITOR_OPTIONS_CLASS"

var HEADERS_NODE_ID = "append_input_headers_here";

// for selection question
// var SELECTION_EDITOR__
var SELECTION_EDITOR__CURRENT_OPTION_NUMBER = 0; // used as static value between  inputEditorSelection() and inputEditorSelectionAddOption()
var SELECTION_EDITOR__OPTION__CLASS = "SELECTION_EDITOR__OPTION__CLASS"; // Options that have associated this string are retrieved as a hole
var SELECTION_EDITOR__OPTIONS__KEY = "SELECTION_EDITOR__OPTION__KEY"
var SELECTION_EDITOR__OPTION_HAS_TEXT__CLASS = "SELECTION_EDITOR__OPTION_HAS_TEXT__CLASS";
var SELECTION_EDITOR__OPTION_HAS_TEXT__KEY = "SELECTION_EDITOR__OPTION_HAS_TEXT__KEY"



// for yesno questions
var YESNO_EDITOR__HAS_THIRD_OPTION__KEY = "YESNO_EDITOR__HAS_THIRD_OPTION_KEY";
var YESNO_EDITOR__DEPENDANT_QUESTIONS__KEY = "YESNO_EDITOR__DEPENDANT_QUESTIONS__KEY";
