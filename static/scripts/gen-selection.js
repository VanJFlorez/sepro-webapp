/**
    Creates the associated input handler to create selection input form elements.
*/
function inputEditorSelection(node) {
    SELECTION_EDITOR__CURRENT_OPTION_NUMBER = 1;

    let div = document.createElement("div");
    div.setAttribute("id", "append_options_here");
    node.appendChild(div);

    let addOption = document.createElement("button");
    addOption.appendChild(document.createTextNode("Nueva opción."));
    addOption.setAttribute("onclick", "inputEditorSelectionAddOption()");
    node.appendChild(addOption);
}
/**
    Called only by inputEditorSelection(), so SELECTION_EDITOR__CURRENT_OPTION_NUMBER is only
    incremented here...
*/
function inputEditorSelectionAddOption() {
    let baseId = "opcion_";
    div = document.getElementById("append_options_here");
    label = document.createElement("label");
    label.setAttribute("for", baseId + SELECTION_EDITOR__CURRENT_OPTION_NUMBER);
    label.appendChild(document.createTextNode("Opción #" + SELECTION_EDITOR__CURRENT_OPTION_NUMBER + ":"));
    div.appendChild(label);
    div.appendChild(document.createElement("br"));

    option = document.createElement("input");
    option.setAttribute("class", SELECTION_EDITOR__OPTION__CLASS);  // lets you retrieve all options in a list.... see getValues()
    option.setAttribute("id", baseId + SELECTION_EDITOR__CURRENT_OPTION_NUMBER); // id used by label tag
    div.appendChild(option);
    SELECTION_EDITOR__CURRENT_OPTION_NUMBER++;

    let checkbox = document.createElement("input");
    checkbox.setAttribute("type", "checkbox");
    checkbox.setAttribute("class", SELECTION_EDITOR__OPTION_HAS_TEXT__CLASS);
    checkbox.setAttribute("onclick", "inputEditorSelectionAddOption_aux(this)")
    div.appendChild(checkbox);

    let txt = document.createElement("strong");
    txt.appendChild(document.createTextNode(" con texto."))
    div.appendChild(txt);
}
/**
 * adds an input text field for the option.
 * Called only by inputEditorSelectionAddOption()
 * @param {Node} node - HTML node where input text will be appended
 */
function inputEditorSelectionAddOption_aux(node) {
    /* Note: if value attribute exists in this element, has the specified value
     *       otherwise has the "on";
     */
    if (node.value == "true")
        node.removeAttribute("value");
    else
        node.setAttribute("value", "true");
}
/**
    All buildElement functions make two calls that sumarizes the creation of forms
    elements. First call to makeRaw to build the form element's container and
    then creates the desired form element.
    After append form elements allways is updated the id global variable.
*/
function buildSelection(type) {
   CURRENT_ID = CURRENT_ID + 1;
    let values = getValuesFromEditor();
    let textLabel = values.get(QUESTION_KEY);
    let options = values.get(SELECTION_EDITOR__OPTIONS__KEY);

    let depQuestionRef = getDependantQuestion(values);
    var body = makeRow(CURRENT_ID, textLabel);
    var selectionElement = null;
    switch (type) {
      case 1: // radio button
        selectionElement = new SelectionElement(type, CURRENT_ID, textLabel, options, depQuestionRef);
        break;
      case 2: // checkbox
        selectionElement = new SelectionElement(type, CURRENT_ID, textLabel, options, depQuestionRef);
        break;
      default:
        console.log("This option is not implemented yet...")
        break;
    }
    body.appendChild(selectionElement.element);
    survey.addFormElement(selectionElement);
}
/**
  * @param {Int} type - Indicates what type of select is.. 1 for radio, 2 for Checkbox
  * @param {Int} baseId - the id upon wich will be constructed the form element. This value
  *                       defines which will be the name for this form element in the server.
  * @param {SelectionYesNo} depQuestionRef - A reference to a yesno question form Element. This
  *                        this warranties the integrity of the relation between dependant
  *                        questions.
  * @param {Array} options - array of arrays... each subarray has two elements:
  *                          0 - any string
  *                          1 - "on" or "true" strings...
  * @returns {DomElement} - <div></div> DOM HTML element
*/
function createSelection(type, baseId, options, depQuestionRef) {
    let id = baseId;
    // form_canvas ~> divRow ~> divCard ~> divCardContent ~> divOptions
    let formElement = document.createElement("div");

    if(depQuestionRef != null)
        formElement.setAttribute("class", depQuestionRef.id + "_derived_hide");
    // form_canvas ~> divRow ~> divCard ~> divCardContent ~> divOptions ~> input
    for(var i = 0; i < options.length; i++) {
        let value = options[i][0]; //prompt("opción " + (i + 1) + ": ");
        let input = document.createElement("input");
        if(type == 1) {
            input.setAttribute("type","radio");
            input.setAttribute("name", id);
            input.setAttribute("inner-id", id + "_" + i); // used only for hidding text input...
        } else {
            input.setAttribute("type", "checkbox");
            input.setAttribute("name", id + "_" + i);
            input.setAttribute("inner-id", id + "_" + i); // used only for hidding text input...
        }
        input.setAttribute("value", value);
        formElement.appendChild(input);
        formElement.appendChild(document.createTextNode(value));

        if(options[i][1] == "true"){
            let hideDiv = document.createElement("div"); // wrapper div that lets hide the text input inside it....
            hideDiv.setAttribute("hidden", "");
            input.setAttribute("onclick", "enableV2(this)"); // callback function that hides the input text....

            let inText = document.createElement("input");
            inText.setAttribute("type", "text");

            if(type == 1) { // set the id accordingly....
                inText.setAttribute("name", id + "_" + i + "_text_v");
                inText.setAttribute("id", id + "_" + i + "_text_v");

                hideDiv.setAttribute("id", id + "_" + i + "_text");
            } else { // set the id accordingliy...
                inText.setAttribute("name", id + "_" + i + "_text_v");
                inText.setAttribute("id", id + "_" + i + "_text_v");

                hideDiv.setAttribute("id", id + "_" + i + "_text");
            }

            hideDiv.appendChild(inText);
            formElement.appendChild(hideDiv);
        }

        formElement.appendChild(document.createElement("br")); // always, after an option append a line break
    }
    return formElement;
}
