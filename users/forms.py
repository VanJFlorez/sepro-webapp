from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import SeproUser

class SeproUserChangeForm(UserChangeForm):
    class Meta:
        model = SeproUser
        fields = ('username', 'email')

class SeproUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = SeproUser
        fields = ('username', 'email')
