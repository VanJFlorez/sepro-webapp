from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model

from allauth.account.signals import user_logged_in
from django.db.models.signals import post_save

'''
    This method is executed whenever an user signs up in the app
'''
def add_user_to_survey_group(sender, instance, created, **kwargs):
    '''
        instance param is an instance of EmailAddress field in django.contrib.auth.db
    '''
    if created:
        User = get_user_model()
        try:
            usr = User.objects.get(username__exact=get_username(instance.__str__()))
            usr.is_superuser = True
            usr.save()
            Group.objects.get(name='survey').user_set.add(usr)
            # instance.groups.add(Group.objects.get(name='survey'))
            print("User added to survey group.")
        except:
            print("ERROR")
'''
    connects the signal
'''
post_save.connect(add_user_to_survey_group)


'''
    This method is called when an user signs up via social account
'''
def add_social_account_to_survey_group(sender, request, user, **kwargs):
    if user is not None:
        try:
            Group.objects.get(name='survey').user_set.add(user)
            user.is_superuser = True
            user.save()
            print("ok ok")
        except:
            print("ERROR2")

'''
    connects the signal
'''
user_logged_in.connect(add_social_account_to_survey_group)
    
'''
    Returns the local part of an email address
'''
def get_username(s):
    username = ""
    for c in s:
        if c != '@':
            username = username + c
        else:
            break
    print(username)
    return username
