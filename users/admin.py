from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import SeproUserCreationForm, SeproUserChangeForm
from .models import SeproUser

class SeproUserAdmin(UserAdmin):
    add_form = SeproUserCreationForm
    form = SeproUserChangeForm
    model = SeproUser
    list_display = ['email', 'username', ]

admin.site.register(SeproUser, SeproUserAdmin)
